#!/bin/sh
#

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
FULLSCREEN=/usr/bin/gtk_fullscreen_process

# only fullscreen if the tool exists
test -x $FULLSCREEN && USE_FULLSCREEN=yes


trap bashtrap SIGHUP

bashtrap()
{
    kill COPROC_PID
}


if [ x"$USE_FULLSCREEN" = xyes ]; then
    {
        sleep 5
        $FULLSCREEN "$@"
    } &
fi

#"$@"
#"$@" 2>&1 > /dev/null
coproc "$@" 2>&1 > /dev/null
wait