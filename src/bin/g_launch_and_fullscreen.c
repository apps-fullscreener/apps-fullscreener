/** @g_launch_and_fullscreen.c
 * \brief Launches a process and sends a fullScreen hardkey event to a
 *        process
 *
 * This program spawns a process and, 5 seconds later, spawns another
 * process to send a fake hard fullscreen key event to the previously
 * spawn process.
 *
 * Copyright (C) 2011, Igalia, S.L.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * Author: Andres Gomez <agomez@igalia.com>
 */

/* To compile:
 gcc -o g_launch_and_fullscreen g_launch_and_fullscreen.c \
 `pkg-config --cflags glib-2.0 gobject-2.0 gthread-2.0` -ansi -Wall \
 `pkg-config --libs glib-2.0 gobject-2.0 gthread-2.0`
 */

#include <glib-object.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <signal.h>


/* gtk_fullscreen_process comes with g_launch_and_fullscreen and is
 * part of apps-fullscreener, so we really don't have to check for its
 * presence in the system */
static const gchar* fullscreen = "gtk_fullscreen_process";

static GThread *launch_thread = NULL;
static GThread *fullscreen_thread = NULL;
static GPid launch_pid;
static GPid fullscreen_pid;
static GMainLoop *main_loop = NULL;


static void
_sigint_handler (int sig)
{
    if (NULL != main_loop) {
        g_main_loop_quit (main_loop);
    }
}


static void
_launch_pid_watch (GPid pid, gint status, gpointer user_data)
{
    if (NULL != main_loop) {
        g_main_loop_quit (main_loop);
    }
}


static gboolean
_fullscreen_process (gchar* process_name)
{
    GError *error = NULL;
    gchar **argv = NULL;
    gchar *tmp = NULL;
    gint argc;
    gboolean result = FALSE;


    tmp = g_strdup_printf ("%s %s", fullscreen, process_name);

    g_shell_parse_argv (tmp, &argc, &argv, &error);

    if (error && (0 != error->code)) {
        goto cleanup;
    }

    if (error) {
        g_error_free (error);
    }

    /* Give time to the other thread to launch the process */
    g_usleep (10 * G_USEC_PER_SEC);

    g_spawn_async (NULL, argv, NULL,
                   G_SPAWN_SEARCH_PATH,
                   NULL, NULL,
                   &fullscreen_pid, &error);

    if (error && (0 != error->code)) {
        goto cleanup;
    }

    result = TRUE;


cleanup:

    g_free (tmp);
    g_strfreev (argv);
    if (error) {
        g_error_free (error);
    }

    return result;
}


static gboolean
_launch_application (gchar **argv)
{
    GError *error = NULL;
    gboolean result = FALSE;
    gchar **new_argv = NULL;


    new_argv = g_strdupv (argv);

    g_spawn_async (NULL, new_argv, NULL,
                   G_SPAWN_SEARCH_PATH | G_SPAWN_DO_NOT_REAP_CHILD,
                   NULL, NULL,
                   &launch_pid, &error);

    if (error && (0 != error->code)) {
        goto cleanup;
    }

    g_child_watch_add (launch_pid, (GChildWatchFunc)_launch_pid_watch, NULL);

    result = TRUE;


cleanup:

    g_strfreev (new_argv);
    if (error) {
        g_error_free (error);
    }

    return result;
}


static void
_quit_gently (void)
{
    kill (launch_pid, SIGHUP);
    kill (fullscreen_pid, SIGHUP);

    g_spawn_close_pid (launch_pid);
    g_spawn_close_pid (fullscreen_pid);

    if (NULL != fullscreen_thread) {
        g_thread_join (fullscreen_thread);
    }
    if (NULL != launch_thread) {
        g_thread_join (launch_thread);
    }
    if (NULL != main_loop) {
        g_main_loop_unref (main_loop);
    }
}


/* main:
 * @argc: number of arguments
 * @argv: arguments
 *
 * Main function
 *
 * @returns: 0 if successful, an error otherwise
 */
int
main (int argc, char **argv)
{
    struct sigaction sa;
    gboolean result = 0;


    g_thread_init (NULL);
    g_type_init();

    sa.sa_handler = _sigint_handler;
    sa.sa_flags = 0; // or SA_RESTART
    sigemptyset(&sa.sa_mask);

    if (sigaction (SIGHUP, &sa, NULL) == -1) {
        result = 1;

        goto cleanup;
    }

    if (argc < 2) {
        result = 2;

        goto cleanup;
    }

    launch_thread = g_thread_create ((GThreadFunc) _launch_application,
                                     &argv[1],
                                     TRUE,
                                     NULL);

    fullscreen_thread = g_thread_create ((GThreadFunc) _fullscreen_process,
                                         argv[1],
                                         TRUE,
                                         NULL);

    main_loop = g_main_loop_new (NULL, FALSE);
    g_main_loop_run (main_loop);


cleanup:

    _quit_gently ();

    return result;
}
