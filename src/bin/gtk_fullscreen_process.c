/** @gtk_fullscreen_process.c
 * \brief Send FullScreen hardkey event to a process
 *
 * This program sends a fake hard fullscreen key event to a certain
 * process.
 *
 * Copyright (C) 2011, Igalia, S.L.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * Author: Andres Gomez <agomez@igalia.com>
 */

/* To compile:
 gcc -o gtk_fullscreen_process gtk_fullscreen_process.c \
 `pkg-config --cflags gtk+-2.0` -ansi -Wall \
 `pkg-config --libs gtk+-2.0`
 */



#include <gtk/gtk.h>
#include <gdk/gdkx.h>
#include <gdk/gdkkeysyms.h>

/* #include <glib/gprintf.h> */

#include <stdlib.h>


/* pidof is part of sysvinit, we really don't have to check for its
 * presence in the system */
static const gchar* pidof = "pidof";

/* #include <hildon/hildon-defines.h> */
/* #define KEY_VAL HILDON_HARDKEY_FULLSCREEN */
/* This avoids Hildon-1 dependency */
#define KEY_VAL GDK_F6


static XKeyEvent
_create_key_event (GdkWindow *win,
                   gboolean press,
                   guint keyval,
                   guint modifiers)
{
   XKeyEvent event;

   event.display = GDK_DISPLAY_XDISPLAY (gdk_display_get_default ());
   event.window = GDK_WINDOW_XWINDOW (win);
   event.root = GDK_WINDOW_XWINDOW (gdk_get_default_root_window ());
   event.subwindow = None;
   event.time = CurrentTime;
   event.x = 1;
   event.y = 1;
   event.x_root = 1;
   event.y_root = 1;
   event.same_screen = TRUE;
   event.keycode = XKeysymToKeycode(event.display, keyval);
   event.state = modifiers;

   if (press) {
      event.type = KeyPress;
   } else {
      event.type = KeyRelease;
   }

   return event;
}


static gboolean
_get_pid_by_name (const gchar* process_name, glong *pid)
{
    GError *error = NULL;
    gchar **argv = NULL;
    gchar *tmp = NULL;
    gchar *standard_output = NULL;
    gint argc;
    gboolean result = FALSE;


    tmp = g_strdup_printf ("%s %s", pidof, process_name);

    g_shell_parse_argv (tmp, &argc, &argv, &error);

    if (error && (0 != error->code)) {
        goto cleanup;
    }

    if (error) {
        g_error_free (error);
    }

    g_spawn_sync (NULL, argv, NULL,
                  G_SPAWN_SEARCH_PATH |
                  G_SPAWN_STDERR_TO_DEV_NULL,
                  NULL, NULL,
                  &standard_output, NULL,
                  NULL, &error);

    if (error && (0 != error->code)) {
        goto cleanup;
    }

    *pid = atoi (standard_output);
    /* g_printf ("PID OF %s: %lu.\n", process_name, *pid); */

    result = TRUE;


cleanup:

    g_free (tmp);
    g_free (standard_output);
    g_strfreev (argv);
    if (error) {
        g_error_free (error);
    }

    return result;
}


/* main:
 * @argc: number of arguments
 * @argv: arguments
 *
 * Main function
 *
 * @returns: 0 if successful, an error otherwise
 */
int
main (int argc, char **argv)
{
    glong pid = -1;
    GdkDisplay *display = NULL;
    GdkScreen *default_screen = NULL;
    GList *window_stack = NULL;
    GList *tmp_list = NULL;
    GdkWindow* current_window = NULL;
    GdkNativeWindow process_wid;
    GdkAtom pid_property;
    GdkAtom property_type;
    XKeyEvent x_key_event;
    gboolean result = 0;

    gtk_init (&argc, &argv);

    if (argc < 2) {
        result = 1;

        goto cleanup;
    }

    if (!_get_pid_by_name (argv[1], &pid) || (-1 == pid)) {
        result = 2;

        goto cleanup;
    }

    pid_property = gdk_atom_intern ("_NET_WM_PID", FALSE);
    property_type = gdk_atom_intern ("CARDINAL", FALSE);

    display = gdk_display_get_default ();
    default_screen = gdk_screen_get_default ();
    window_stack = gdk_screen_get_window_stack (default_screen);

    tmp_list = window_stack;
    while (tmp_list) {
        gulong property_offset = 0;
        gulong property_length = 1;
        gint property_delete = FALSE;
        gint property_actual_length;
        guchar *property_data = NULL;
        gboolean ok = FALSE;


        current_window = GDK_WINDOW (tmp_list->data);

        ok = gdk_property_get (current_window, pid_property, property_type,
                               property_offset, property_length, property_delete,
                               NULL, NULL,
                               &property_actual_length, &property_data);

        if (ok) {
            if (property_data) {
                gboolean found = (pid == *((glong *) property_data));

                g_free (property_data);

                if (found) {
                    process_wid =
                        (GdkNativeWindow) GDK_WINDOW_XWINDOW (current_window);
                    gdk_window_focus (current_window, CurrentTime);
                    /* g_printf ("FOUND!!!.\n"); */
                    /* g_printf ("%s window XID is %lu.\n", */
                    /*           argv[1], */
                    /*           GDK_WINDOW_XID (current_window)); */

                    break;
                }
            }

            /* g_printf ("NOT FOUND!!!.\n"); */
        }

        tmp_list = g_list_next (tmp_list);
    }

    /* FIXME: This is not totally working as it resizes to fullscreen,
     * but the inner windows are not resized too, at least with the
     * skype app. Maybe something is missing :( */
    /* gdk_window_fullscreen (current_window); */
    /* gdk_window_maximize (current_window); */

    /* Hence, we send a fake fullscreen hard key event to the
     * GdkWindow */
    x_key_event = _create_key_event (current_window, TRUE, KEY_VAL, 0);
    XSendEvent (x_key_event.display, x_key_event.window,
                TRUE, KeyPressMask, (XEvent *) &x_key_event);

    x_key_event = _create_key_event (current_window, FALSE, KEY_VAL, 0);
    XSendEvent (x_key_event.display, x_key_event.window,
                TRUE, KeyPressMask, (XEvent *) &x_key_event);


cleanup:

    g_list_free (window_stack);
    gdk_display_close (display);

    return result;
}
